﻿#include <iostream>
#include <string>

int main()
{   //вывод строки
    std::string line = "Ya lublu buterbrot s kolbasoi";
    std::cout << "Stroka - "<< line << "\n";

    //вывод длины строки
    std::cout << "Dlina stroki = "<< line.length() << "\n";

    //вывод первого символа строки
    char s1 = line[0];
    std::cout << "Pervyi simvol - "<< s1 << "\n";

    //вывод псоледнего символа строки
    char sLast = line[line.length()-1];
    std::cout << "Poslednii simvol - "<< sLast << "\n";

    std::cin;
    return 0;
}

